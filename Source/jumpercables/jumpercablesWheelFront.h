// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "jumpercablesWheelFront.generated.h"

UCLASS()
class UjumpercablesWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UjumpercablesWheelFront();
};



