// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "Vehicles/VehicleWheel.h"
#include "jumpercablesWheelRear.generated.h"

UCLASS()
class UjumpercablesWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UjumpercablesWheelRear();
};



